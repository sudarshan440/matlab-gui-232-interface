

function varargout = gui_try3(varargin)
% GUI_TRY3 M-file for gui_try3.fig
%      GUI_TRY3, by itself, creates a new GUI_TRY3 or raises the existing
%      singleton*.
%
%      H = GUI_TRY3 returns the handle to a new GUI_TRY3 or the handle to
%      the existing singleton*.
%
%      GUI_TRY3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TRY3.M with the given input arguments.
%
%      GUI_TRY3('Property','Value',...) creates a new GUI_TRY3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_try3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_try3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_try3

% Last Modified by GUIDE v2.5 10-Aug-2015 12:40:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_try3_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_try3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_try3 is made visible.
function gui_try3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_try3 (see VARARGIN)
serialPorts = instrhwinfo('serial');
set(handles.listbox1, 'String', ...
    [{'Select a port'} ; serialPorts.SerialPorts ]);
set(handles.listbox1, 'Value', 2);   

set(handles.rxlogbox, 'String', cell(1));
% Choose default command line output for gui_try3
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui_try3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_try3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function baudratevalue_Callback(hObject, eventdata, handles)
% hObject    handle to baudratevalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of baudratevalue as text
%        str2double(get(hObject,'String')) returns contents of baudratevalue as a double


% --- Executes during object creation, after setting all properties.
function baudratevalue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baudratevalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'connect') % currently disconnected
    disp('initial valus of serportn is ');
    serPortn = get(handles.listbox1, 'Value');
    if serPortn == 1
        errordlg('Select valid COM port');
    else
        serList = get(handles.listbox1,'String');
        serPort = serList{serPortn};

        
%         serConn = serial(serPort, 'TimeOut', 1, ...
%             'BaudRate', str2num(get(handles.baudratevalue, 'String')))
                serConn = serial(serPort,'BaudRate', str2num(get(handles.baudratevalue, 'String')));
               
        try
          
        

        
            handles.serConn = serConn;
            set(handles.serConn, 'Terminator','CR');
            set(handles.serConn, 'Timeout',0.5);
                fopen(serConn)
            % enable Tx text field and Rx button
            set(handles.txsend, 'Enable', 'On');
            set(handles. rxtoreceive, 'Enable', 'On');
            
%             set(hObject, 'String','Disconnect')
        catch e
            errordlg(e.message);
        end
        
    end
else
%         set(handles.txsend, 'Enable', 'Off');
%     set(handles. rxtoreceive, 'Enable', 'Off');
    
    set(hObject, 'String','Connect')
%     fclose(handles.serConn);
end

guidata(hObject, handles);

% --- Executes on selection change in rxlogbox.
function rxlogbox_Callback(hObject, eventdata, handles)
% hObject    handle to rxlogbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns rxlogbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rxlogbox


% --- Executes during object creation, after setting all properties.
function rxlogbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rxlogbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txsend_Callback(hObject, eventdata, handles)


valuetoprint = get(handles.txsend,'String');
handles.serConn
fprintf(handles.serConn,valuetoprint);

currList = get(handles.rxlogbox, 'String');

set(handles.rxlogbox, 'String', ...
    [currList ; ['Sent @ ' datestr(now) ': ' valuetoprint] ]);
set(handles.rxlogbox, 'Value', length(currList) +1 );
set(hObject, 'String', '');
% hObject    handle to txsend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txsend as text
%        str2double(get(hObject,'String')) returns contents of txsend as a double


% --- Executes during object creation, after setting all properties.
function txsend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txsend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in rxtoreceive.
function rxtoreceive_Callback(hObject, eventdata, handles)
% hObject    handle to rxtoreceive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('in rxmodule')
try
valuefromrx = fscanf(handles.serConn)
currList = get(handles.rxlogbox, 'String');
set(handles.rxlogbox, 'String', ...
    [currList ; ['receive @ ' datestr(now) ': ' valuefromrx] ]);
set(handles.rxlogbox, 'Value', length(currList) +1 );
catch e
    disp(e);
end
% --- Executes during object creation, after setting all properties.
function rxtoreceive_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rxtoreceive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function terminate_Callback(hObject, eventdata, handles)
% hObject    handle to terminate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.txsend, 'Enable', 'Off');
    set(handles. rxtoreceive, 'Enable', 'Off');


    fclose(handles.serConn);



function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'serConn')
    fclose(handles.serConn);
end
% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in terminate.

